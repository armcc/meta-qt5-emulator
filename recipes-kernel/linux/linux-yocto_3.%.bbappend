# Copyright (c) 2013 LG Electronics, Inc.

# Assign to PE, because empty PKGE in KERNEL_IMAGE_BASE_NAME causes two hyphens.
PE = "1"

FILESEXTRAPATHS_prepend := "${THISDIR}/linux-yocto:"

SRC_URI_append_qemuall = " file://my_gfx.cfg \
                           file://enable_uinput.cfg \
                         "
