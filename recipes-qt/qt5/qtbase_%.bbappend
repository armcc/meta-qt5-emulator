
FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += "\
    file://eglfswindow.patch \
"

# ----------------------------------------------------------------------

PACKAGECONFIG_GL = "gles2"

EXTRA_OECONF_qemux86 += "-qpa eglfs"
EXTRA_OECONF_qemux86-64 += "-qpa eglfs"

MESA_RDEPENDS = "mesa-driver-swrast"
RDEPENDS_${PN}_append_qemux86 = " ${MESA_RDEPENDS}"
RDEPENDS_${PN}_append_qemux86-64 = " ${MESA_RDEPENDS}"
