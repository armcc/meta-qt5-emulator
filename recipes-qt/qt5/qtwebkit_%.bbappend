
RDEPS_WEBKIT_MEDIAPLAYER ?= ""
RDEPS_WEBKIT_MEDIAPLAYER_append_qemux86 = " gst-plugins-base-app gst-ffmpeg"
RDEPS_WEBKIT_MEDIAPLAYER_append_qemux86-64 = " gst-plugins-base-app gst-ffmpeg"

RDEPENDS_${PN} += "\
    ${RDEPS_WEBKIT_MEDIAPLAYER} \
"
