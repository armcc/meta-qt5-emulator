# Use gallium and llvmpipe for rendering
PACKAGECONFIG_append_qemuall = " gallium gallium-egl gallium-gbm gallium-llvm"

# We want the fbdev platform as well
EGL_PLATFORMS_append_qemuall = ",fbdev"
